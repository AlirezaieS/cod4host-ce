### Contribution Guide:

**Remember that this project is under development.**

  * Submit messages on commits.

  * Try to use official git app, unofficial applications may produce extra unwanted files, or edit the `.gitignore` instead.

  * All members with access should fork the repository fand submit merge request.
